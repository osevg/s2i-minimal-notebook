#!/bin/bash

JUPYTER_NOTEBOOK_DIR=${JUPYTER_NOTEBOOK_DIR:-/opt/app-root/src}

JUPYTER_NOTEBOOK_CONFIG=/opt/app-root/jupyter_notebook_config.py

touch $JUPYTER_NOTEBOOK_CONFIG

cat >> $JUPYTER_NOTEBOOK_CONFIG << !
import os

password = os.environ.get('JUPYTER_USER_PASSWORD')

if password:
    import notebook.auth

    c.NotebookApp.password = notebook.auth.passwd(password)

    del password
    del os.environ['JUPYTER_USER_PASSWORD']
!

# We use some magic here with background processes and signals as the
# Jupyter notebook instances can't run as process ID 1. We leave this
# script running as process ID 1 and leave it to handle any zombie
# processes but must use the dance below to ensure signals will still be
# propagated correctly to Jupyter notebook so shutdown is clean.

trap 'kill -TERM $PID' TERM INT

exec jupyter notebook --no-browser --debug --log-level=DEBUG \
    --ip=* --port=8080 --config=$JUPYTER_NOTEBOOK_CONFIG \
    --notebook-dir=$JUPYTER_NOTEBOOK_DIR &

PID=$!
wait $PID
trap - TERM INT
wait $PID
STATUS=$?
exit $STATUS
