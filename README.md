# Minimal Jupyter Notebook

This repository provides sample code for creating a minimal Python notebook image. It is created using the default S2I Python builder of OpenShift. The resulting Python image can be run as is to create an empty workspace, or can be invoked as a S2I builder against a repository to incorporate a set of notebooks into an application image, which can then be run.

## Implementation Notes

This sample uses a custom ``.s2i/bin/assemble`` script to install a custom application script file for starting up the final Jupyter notebook instance. A ``.s2i/environment`` file is used to specify the final location of the startup script via the ``APP_FILE`` environment variable. This environment variable will be added to the image itself so will survive subsequent use of the image as a S2I builder.

## Deployment Steps

To create an image from this repository inside of OpenShift, using the ``oc`` command line tool, you can run:

```
oc new-build https://gitlab.com/osevg/s2i-minimal-notebook.git
```

In this case, because no language type was specified, OpenShift will determine the language by inspecting the code repository. Because the code repository contains a ``requirements.txt``, it will subsequently be interpreted as including a Python application. When such automatic detection is used, ``python:latest`` will be used.

Use of ``python:latest`` is the same as having selected the most up to date Python version available, which at this time is ``python:3.4``. If you want to use Python 2.7, you should instead run:

```
oc new-build python:2.7~https://gitlab.com/osevg/s2i-minimal-notebook.git
```

In either case, the resulting image will be called ``s2i-minimal-notebook``.

To run this image with an empty workspace, you can run the command:

```
oc new-app s2i-minimal-notebook --name notebook
oc expose svc/notebook
```

Replace ``notebook`` as the argument to ``--name``, and in service name to ``oc expose``, with the name you wish to assign to the running instance.

When finished, you can delete the notebook instance by running:

```
oc delete all --selector app=notebook
```

For case of an empty workspace, you will need to upload any notebooks yourself, or start from scratch. Anything you create will not be persistent, so if the instance is restarted you will loose your data. If you require your work to be persistent, before doing anything, you can mount a persistent volume on the directory ``/opt/app-root/src``.

```
oc set volume dc/notebook --add --claim-size 512M --claim-name notebooks --mount-path /opt/app-root/src --name notebooks
```

By default the notebook instance will not be password protected. If you want the use of a password to be enabled, you should set the ``JUPYTER_USER_PASSWORD`` environment variable when running ``oc new-app``.

```
oc new-app s2i-minimal-notebook --name notebook --env JUPYTER_USER_PASSWORD=secret
```

You may wish to ensure you expose the notebook instance over HTTPS when using a password. This can be done using ``oc create route`` instead of ``oc expose``.

```
oc create route edge --service notebook
```

Rather than start with an empty workspace, if you want to pre-populate the workspace with notebooks and data files from a remote Git repository, you can run the ``s2i-minimal-notebook`` image as a S2I builder.

```
oc new-app s2i-minimal-notebook~https://github.com/ipython/ipython-in-depth.git --name examples
```

Note that as this is a minimal notebook, not all notebooks in the repository referenced in this example will work. Additional Python packages would need to be installed.

If you want to have additional Python packages installed which are needed by a set of notebooks, you can add a ``requirements.txt`` file to the root of the repository containing the notebooks and the packages will be automatically installed when the image is being built.

